import logging
import pathlib
import pytest
import yaml
from pauto.instrumenteddevice import InstrumentedDevice
import pauto.kwlib.common


def pytest_addoption(parser):
    parser.addoption('--loglevel', default=None, type=int)
    parser.addoption('--model', default='default')
    parser.addoption('--serialno', default=None, type=str)


def pytest_ignore_collect(path, config):
    base_path = pathlib.Path(__file__).resolve().parent
    path_parts = pathlib.Path(str(path)).relative_to(base_path).parts
    model_name = config.getoption('--model')
    if model_name in path_parts:
        return False
    if 'common' in path_parts:
        return False
    return True


@pytest.fixture(scope='module')
def device(request):
    # LOAD MODEL 
    model_name = request.config.getoption('--model')
    base_path = pathlib.Path(__file__).resolve().parent.parent
    modelconf_path = base_path.joinpath('models', model_name + '.yml')
    with modelconf_path.open() as modelconf_file:
       modelconf = yaml.load(modelconf_file)

    # SET LOGLEVEL
    loglevel = request.config.getoption('--loglevel')
    if loglevel:
        logging.basicConfig(level=loglevel)

    serialno = request.config.getoption('--serialno')
    return InstrumentedDevice(
        model=modelconf,
        serialno=serialno)
