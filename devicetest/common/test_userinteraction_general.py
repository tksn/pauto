import re
import time
import uuid
import pytest
from pauto.precondition import focused_app, verify_condition
from pauto.kwlib.common.userinteractionutil import start_new
from testselect import run_if_model_has


@run_if_model_has('youtube')
def test_stop_app(device):
    device.stop_app('com.google.android.youtube')
    with pytest.raises(RuntimeError):
        device.get_pid('com.google.android.youtube')


@run_if_model_has('apps_button_with_description')
def test_go_home(device):
    device.uiautomator.press.recent()
    device.go_home()
    assert device.uiautomator(description='Apps').exists


@run_if_model_has('maps')
def test_tap_on_text(device):
    device.go_home()
    device.tap_on_text('Google')
    assert device.uiautomator(text='Maps').exists


def test_switch_to_previous_activity(device):
    device.uiautomator.press.home()
    device.launch_calculator()
    device.launch_clock()
    device.switch_to_previous_activity()
    assert 'Calculator' in device.get_focused_app()


@run_if_model_has('stock_calculator', 'stock_clock')
def test_switch_to_window(device):
    device.uiautomator.press.home()
    device.launch_calculator()
    device.launch_clock()
    device.switch_to_window('Calculator')
    assert 'Calculator' in device.get_focused_app()


@run_if_model_has('browser')
def test_open_browser(device):
    url = 'http://www.bbc.com'
    device.open_browser(url)
    verify_condition(
        focused_app('com.android.browser/.BrowserActivity'),
        device)


@run_if_model_has('browser')
def test_browser_scroll(device):
    # Historical Netscape website, unchanged since 1994
    url = 'http://home.mcom.com/home/welcome.html'
    device.open_browser(url)
    device.browser_scroll()
    assert device.uiautomator(description='Copyright © 1994 Mosaic Communications Corporation.').exists


@run_if_model_has('stock_dialer', 'iphonesubinfo_13_phonenumber')
def test_make_call_and_hangup(device):
    myself = device.get_phone_number()
    device.make_call(myself)
    device.uiautomator(resourceId='com.android.dialer:id/endCallButton').exists
    device.hangup_call()
    assert device.uiautomator(resourceId='com.android.dialer:id/endCallButton').wait.gone()


@run_if_model_has('stock_messaging')
def test_send_sms(device):
    myself = device.get_phone_number()
    tickerStr0 = str(uuid.uuid4())
    device.send_sms(myself, tickerStr0)
    tickerStr1 = str(uuid.uuid4())
    device.send_sms(myself, tickerStr1)
    assert device.uiautomator(text=tickerStr0).exists

