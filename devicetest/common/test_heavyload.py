from testselect import run_if_model_has


def test_force_low_ram(device):
    RAM_MB = 1400
    device.force_low_ram(RAM_MB, used_packages=[
        'com.android.gallery',
        'com.android.quicksearchbox',
        'com.android.browser',
        'com.android.calendar',
        'com.android.webview',
        'com.android.launcher3',
        'com.android.calculator2'])
    assert device.get_meminfo()['available'] < RAM_MB * 1024
