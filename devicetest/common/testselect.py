import functools
import pytest


def run_if_model_has(*traits):

    def decorator_func(original_func):

        @functools.wraps(original_func)        
        def decorated_func(device, *args, **kwargs):
            model = device.model
            if 'noskip' in kwargs:
                kwa = dict(kwargs)
                del kwa['noskip']
                original_func(device, *args, **kwa)
                return
            if not frozenset(traits) < frozenset(model['traits']):
                pytest.skip('not applicable to this model')
            original_func(device, *args, **kwargs)
        return decorated_func
    return decorator_func
