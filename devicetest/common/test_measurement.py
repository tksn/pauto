from testselect import run_if_model_has


@run_if_model_has('playstore_on_home')
def test_measure_launch_time(device):
    device.stop_app('com.android.vending')
    device.go_home()
    device.uiautomator.wait.idle()
    result = device.measure_launch_time('Play Store')
    assert result > 0.0

