from testselect import run_if_model_has


def test_get_package_list(device):
    pkgs = device.get_package_list()
    assert 'com.android.systemui' in pkgs


def test_get_pid(device):
    assert len(device.get_pid('com.android.systemui')) > 0


def test_get_focused_app(device):
    assert len(device.get_focused_app().split('/')) == 2


def test_get_meminfo(device):
    mi = device.get_meminfo()
    assert mi['available'] > 0
    assert mi['total'] > 0
    assert mi['total'] >= mi['available']


@run_if_model_has('playmusic')
def test_get_mediaplayer_state(device):
    playmusicmain = 'com.google.android.music:main'
    audio = device.get_mediaplayer_state(playmusicmain)
    assert audio == 'stop' or audio == 'pause'


@run_if_model_has('audio_device')
def test_get_audio_state(device):
    state = device.get_audio_state('com.android.systemui')
    assert state != 'active'


@run_if_model_has('iphonesubinfo_1_imei')
def test_get_imei(device):
    assert len(device.get_imei()) > 0


@run_if_model_has('iphonesubinfo_13_phonenumber')
def test_get_phone_number(device):
    assert len(device.get_phone_number()) > 0

