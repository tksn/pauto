from pauto.precondition import focused_app, verify_condition
from pauto.kwlib.common.userinteractionutil import wait_until
from testselect import run_if_model_has


@run_if_model_has('twitter')
def test_open_scroll_twitter(device):
    twitter = 'com.twitter.android'
    device.stop_app(twitter)
    device.open_twitter()
    wait_until(lambda: twitter in device.get_focused_app())
    device.twitter_scroll_timeline(5.0)


@run_if_model_has('smartrecorder')
def test_open_record_smartrecorder(device):
    soundrecorder = 'com.andrwq.recorder'
    device.stop_app(soundrecorder)
    device.open_soundrecorder()
    wait_until(lambda: soundrecorder in device.get_focused_app())
    device.soundrecorder_start_recording()
    wait_until(lambda: device.get_audio_state(soundrecorder) == 'active')
    device.soundrecorder_stop_recording()
