from pauto.precondition import focused_app, verify_condition
from pauto.kwlib.common.userinteractionutil import wait_until
from testselect import run_if_model_has


@run_if_model_has('youtube')
def test_open_and_pause_youtube(device):
    url = 'https://www.youtube.com/watch?v=jNQXAC9IVRw'
    youtube = 'com.google.android.youtube'
    device.stop_app(youtube)
    device.open_youtube(url)
    wait_until(lambda: device.get_audio_state(youtube) == 'active')
    device.youtube_pause()
    wait_until(lambda: device.get_audio_state(youtube) != 'active')


@run_if_model_has('playmusic')
def test_open_play_stop_playmusic(device):
    playmusic = 'com.google.android.music'
    playmusicmain = playmusic + ':main'
    device.stop_app(playmusic)
    device.open_playmusic()
    wait_until(lambda: playmusic in device.get_focused_app())
    device.playmusic_play()
    wait_until(lambda: device.get_mediaplayer_state(playmusicmain) == 'play', timeout=60)
    device.playmusic_stop()
    wait_until(lambda: device.get_mediaplayer_state(playmusicmain) == 'stop')


@run_if_model_has('maps')
def test_open_swimon_maps(device):
    maps = 'com.google.android.apps.maps'
    geo_uri = 'geo:38.958157,-119.942874'
    device.stop_app(maps)
    device.open_maps(geo_uri)
    wait_until(lambda: maps in device.get_focused_app())
    device.maps_random_swim(5.0)


@run_if_model_has('chrome')
def test_open_scroll_chrome(device):
    chrome = 'com.android.chrome'
    device.stop_app(chrome)
    device.open_chrome('http://www.cbsnews.com')
    wait_until(lambda: chrome in device.get_focused_app())
    device.chrome_scroll()


@run_if_model_has('playstore')
def test_playstore_install_app(device):
    app = 'com.todoist'
    playstore = 'com.android.vending'
    device.stop_app(playstore)
    device.playstore_install_app(app)
    assert app in device.get_package_list()
    device.uninstall(app)
