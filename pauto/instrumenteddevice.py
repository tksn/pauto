import uiautomatorminus as uiautomator
from . import adb, keyword


class InstrumentedDevice(adb.Device):

    def __init__(self, model, serialno=None):
        super().__init__(serialno)
        self.__model = model
        self.__uiautomator = uiautomator.Device(self.serialno)

    @property
    def model(self):
        return self.__model

    @property
    def uiautomator(self):
        return self.__uiautomator

    def call(self, name, *args, **kwargs):
        kw = dict(kwargs)
        pt = kw.pop('priority_traits', [])
        func = keyword.get_keyword_func(name, self.model, pt)
        return func(self, *args, **kw)

    def __getattr__(self, attr):
        func = keyword.get_keyword_func(attr, self.model)

        def _wrap(*args, **kwargs):
            return func(self, *args, **kwargs)
        return _wrap
