from pauto.keyword import keyword
from pauto.precondition import precondition, focused_app
from .userinteractionutil import start_view, start_new, wait_app_focused, scroll_whole_contents


@keyword()
def stop_app(device, app_package):
    device.shell(['am', 'force-stop', app_package]).wait()


@keyword()
def go_home(device):
    device.shell(['input', 'keyevent', 'KEYCODE_HOME']).wait()
    device.uiautomator.wait.idle()


@keyword()
def tap_on_text(device, text):
    device.uiautomator(text=text).click.wait()


@keyword(require='stock_calculator')
def launch_calculator(device):
    start_new(device, 'com.google.android.calculator/com.android.calculator2.Calculator')


@keyword(name='launch_calculator', require='stock_calculator_old')
def launch_old_calculator(device):
    start_new(device, 'com.android.calculator2/.Calculator')


@keyword(require='stock_clock')
def launch_clock(device):
    start_new(device, 'com.google.android.deskclock/com.android.deskclock.DeskClock')


@keyword(name='launch_clock', require='stock_clock_old')
def launch_old_calculator(device):
    start_new(device, 'com.android.deskclock/.DeskClock')


@keyword(require='nougat_recentview')
@precondition(focused_app('com.android.systemui/.recents.RecentsActivity'))
def recent_scroll_to_end(device):
    device.uiautomator(className='android.widget.ScrollView').scroll.toEnd(steps=20)


@keyword(require='nougat_recentview')
@precondition(focused_app('com.android.systemui/.recents.RecentsActivity'))
def recent_scroll_to_window(device, window_title):
    device.uiautomator(className='android.widget.ScrollView').scroll.to(text=window_title)


@keyword(require='nougat_recentview')
@precondition(focused_app('com.android.systemui/.recents.RecentsActivity'))
def recent_get_windows(device):
    return device.uiautomator(resourceId='com.android.systemui:id/task_view_thumbnail')


@keyword()
def switch_to_previous_activity(device):
    device.uiautomator.press.recent()
    device.uiautomator.wait.idle()
    device.recent_scroll_to_end()
    device.uiautomator.wait.idle()

    activity_desc_obj = device.recent_get_windows()
    elem_count = len(activity_desc_obj)
    if elem_count < 2:
        return
    act_bounds = []
    for i in range(elem_count):
        act_bounds.append(activity_desc_obj[i].info.get('bounds', {}))
    sorted_acts = sorted(act_bounds, key=lambda a: a.get('top', -1), reverse=True)
    second_to_bottom = sorted_acts[1]
    click_x = second_to_bottom['left'] + (second_to_bottom['right'] - second_to_bottom['left']) / 2
    click_y = sorted_acts[1]['top'] + 5
    device.uiautomator.click(click_x, click_y)
    device.uiautomator.wait.idle()


@keyword()
def switch_to_window(device, window_title):
    device.uiautomator.press.recent()
    device.uiautomator.wait.idle()
    device.recent_scroll_to_window(window_title)
    device.uiautomator.wait.idle()
    device.uiautomator(text=window_title).click.wait()


# Browser
BROWSER_MAIN_ACTIVITY = 'com.android.browser/.BrowserActivity'


@keyword(require='browser')
def open_browser(device, url):
    start_view(device, url)
    wait_app_focused(device, BROWSER_MAIN_ACTIVITY)


@keyword(require='browser')
@precondition(focused_app(BROWSER_MAIN_ACTIVITY))
def browser_scroll(device):
    scroll_whole_contents(
        device,
        selector={'resourceId': 'com.android.browser:id/main_content'},
        direction='toEnd')


# Phone calling
DIALER_INCALL_ACTIVITY = 'com.android.dialer/com.android.incallui.InCallActivity'


@keyword(require='stock_dialer')
def make_call(device, number_str):
    device.shell([
        'am', 'start',
        '-a', 'android.intent.action.CALL',
        '-d', 'tel:' + number_str]).wait()


@keyword(require='stock_dialer')
def hangup_call(device):
    device.shell(['input', 'keyevent', 'KEYCODE_ENDCALL']).wait()


@keyword(require='stock_dialer')
def answer_to_incoming_call(device):
    device.shell(['input', 'keyevent', 'KEYCODE_CALL']).wait()


# SMS
def stock_messaging_send_sms_common(device, number, message, activity):
    device.shell([
        'am', 'start', '-a', 'android.intent.action.SENDTO',
        '-d', 'sms:' + number,  '--es', 'sms_body', '"{}"'.format(message),
        '--ez', 'exit_on_sent', 'true',
        '-f', '0x00008000', '-W']).wait()
    wait_app_focused(device, activity)


@keyword(require='stock_messaging')
def send_sms(device, number, message):
    stock_messaging_send_sms_common(
        device, number, message,
        'com.google.android.apps.messaging/.ui.conversation.ConversationActivity')
    device.uiautomator(description='Send SMS').click.wait()


@keyword(name='send_sms', require='stock_messaging_old')
def send_sms_old(device, number, message):
    stock_messaging_send_sms_common(
        device, number, message,
        'com.android.messaging/.ui.conversation.ConversationActivity')
    device.shell(['input', 'keyevent', 'KEYCODE_DPAD_DOWN']).wait()
    device.shell(['input', 'keyevent', 'KEYCODE_DPAD_RIGHT']).wait()
    device.shell(['input', 'keyevent', 'KEYCODE_ENTER']).wait()
