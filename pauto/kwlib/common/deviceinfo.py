import io
import re
from pauto.keyword import keyword


@keyword()
def get_package_list(device):
    outs, _ = device.shell(['pm', 'list', 'packages']).wait()
    lines = (line.strip().replace('package:', '') for line in outs.splitlines())
    return list(line for line in lines if len(line) > 0)


@keyword()
def get_pid(device, package):
    outs, _ = device.shell(['ps']).wait()
    m = re.search(r'(\S+\s+)(\S+\s+){6}' + package.replace('.', r'\.'), outs)
    if not m:
        raise RuntimeError('can not find the package {}'.format(package))
    return m.group(1).strip()


@keyword()
def get_focused_app(device):
    outs, _ = device.shell(['dumpsys', 'window', 'windows']).wait()
    m = re.search(r'mFocusedApp=.+ActivityRecord{\S+ \S+ (\S+) \S+}', outs)
    if not m:
        raise RuntimeError('"dumpsys window windows" did not yield mFocusedApp information')
    return m.group(1)


@keyword()
def get_mediaplayer_state(device, package):
    try:
        pid = get_pid(device, package)
    except RuntimeError as e:
        if 'can not find the package' in str(e):
            return 'stop'
        raise
    outs, _ = device.shell(['dumpsys', 'media.player']).wait()
    pattern = r'pid\(' + pid + r'\).+?AudioTrack::dump.+?state\((\d+)\)'
    search_result = re.search(
        pattern, outs,
        flags=re.MULTILINE | re.DOTALL)
    if not search_result:
        return 'stop'
    known_state = {'0': 'play', '2': 'pause'}
    subgrp = search_result.group(1)
    return known_state.get(subgrp, 'unknown({})'.format(subgrp))


@keyword(require=['audio_device'])
def get_audio_state(device, package):
    pid = get_pid(device, package)
    outs, _ = device.shell(['dumpsys', 'media.audio_flinger']).wait()
    m = re.search(r'(yes|no){1}\s+' + pid, outs)
    if not m:
        return 'not registered'
    return 'active' if m.group(1) == 'yes' else 'inactive'


@keyword()
def get_call_state(device):
    outs, _ = device.shell(['dumpsys', 'telephony.registry']).wait()
    search_result = re.search(r'mCallState=(\d+)', outs, flags=re.MULTILINE)
    if not search_result:
        raise RuntimeError('"dumpsys telephony.registry" did not include mCallState')
    known_state = {'0': 'idle', '1': 'ringing', '2': 'active'}
    subgrp = search_result.group(1)
    return known_state.get(subgrp, 'unknown({})'.format(subgrp))


@keyword()
def get_meminfo(device):
    outs, _ = device.shell(['cat', '/proc/meminfo']).wait()
    lines = (line.strip() for line in outs.splitlines())

    def _get_item(line):
        parts = line.split()
        if len(parts) < 2:
            raise ValueError('unexpected output value: ' + line)
        return (parts[0].strip(':'), int(parts[1]))

    meminfo = dict(_get_item(line) for line in lines if len(line) > 0)
    if any(item not in meminfo for item in ('MemTotal', 'MemFree', 'Cached')):
        raise ValueError('unexpected output value (lacking necessary info)')
    return {
        'total': meminfo['MemTotal'],
        'available': meminfo['MemFree'] + meminfo['Cached']}


@keyword(name='get_imei', require=['iphonesubinfo_1_imei'])
def get_imei_using_iphonesubinfo_1(device):
    return _get_iphonesubinfo(device, 1)


@keyword(name='get_phone_number', require=['iphonesubinfo_13_phonenumber'])
def get_phone_number_using_iphonesubinfo_13(device):
    return _get_iphonesubinfo(device, 13)


def _parse_iphonesubinfo(subinfo_str):

    def _get_data_iter():
        pattern = re.compile(r'[0-9a-f]{8}$')
        for line in io.StringIO(subinfo_str).readlines():
            for data in line.split():
                if not pattern.match(data):
                    continue
                yield int(data[4:], base=16)
                yield int(data[:4], base=16)

    data_iter = _get_data_iter()
    for i in range(4):
        next(data_iter)
    return ''.join((chr(data) for data in data_iter if 0 < data < 256))


def _get_iphonesubinfo(device, code):
    outs, _ = device.shell(['service', 'call', 'iphonesubinfo', str(code)]).wait()
    return _parse_iphonesubinfo(outs)
