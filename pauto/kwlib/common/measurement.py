import pathlib
import logging
from pauto.keyword import keyword


LAUNCHTIME_APKNAME = 'LaunchTimeTapText.apk'
LAUNCHTIME_CLASSNAME = 'org.bitbucket.tksn.lowramtest.LaunchTimeTapText'


@keyword()
def measure_launch_time(device, text, selector=None):
    WAITFOREXIST_TIMEOUT = 30000
    RESPONSE_TIMEOUT = WAITFOREXIST_TIMEOUT + 20000
    args_dict = {
        'text': text,
        'timeout': str(WAITFOREXIST_TIMEOUT),
        'selector': 'clickable',
        'selectorValue': 'true'
        }
    if selector is not None:
        args_dict['selector'] = selector['selector']
        args_dict['selectorValue'] = selector['value']

    apk_path = pathlib.Path(__file__).resolve().parent.joinpath(LAUNCHTIME_APKNAME)
    result = device.uiautomator.remote_exec_apk(
        str(apk_path), LAUNCHTIME_CLASSNAME, args_dict, timeout=RESPONSE_TIMEOUT)
    error = result.get('error')
    if error:
        raise RuntimeError('Remote tap_on_text execution failed: ' + error)
    measure_result = result.get('result')
    if measure_result is None:
        raise RuntimeError('Remote tap_on_text execution failed (returned None)')
    logging.debug('measurement.measure_launch_time: {time_clickAndWait}, {result}'.format(**result))
    return float(measure_result) / 1000

