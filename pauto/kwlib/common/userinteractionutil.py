import time


def start_view(device, data_uri):
    device.shell([
        'am', 'start',
        '-a', 'android.intent.action.VIEW',
        '-d', data_uri]).wait()


def start_new(device, component):
    device.shell([
        'am', 'start',
        '-n', component,
        '-f', '0x00008000', '-W'
    ]).wait()


def wait_app_focused(device, app_activity, timeout=20):
    wait_until(lambda: device.get_focused_app() == app_activity)


def wait_until(event_func, timeout=20):
    start_time = time.time()
    while time.time() - start_time < timeout:
        if event_func():
            return
        time.sleep(0.25)
    raise RuntimeError('wait_until: timeout')


def scroll_period_of_time(device, selector, duration, direction='forward', timeout_sec=60, interval_sec=1.0):
    timeout_msec = timeout_sec * 1000
    scrollarea = device.uiautomator.timeout(timeout_msec)(**selector)
    sclollfunc = getattr(scrollarea.scroll, direction)
    start_time = time.time()
    while time.time() - start_time < duration:
        sclollfunc(steps=20)
        time.sleep(interval_sec)


def scroll_whole_contents(device, selector, direction='toEnd', timeout_sec=60):
    timeout_msec = timeout_sec * 1000
    scrollarea = device.uiautomator.timeout(timeout_msec)(**selector)
    scrollfunc = getattr(scrollarea.scroll, direction)
    scrollfunc(steps=20)
