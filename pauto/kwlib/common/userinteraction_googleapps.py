import random
import time
from pauto.keyword import keyword
from pauto.precondition import precondition, focused_app
from .userinteractionutil import start_view, wait_app_focused, scroll_whole_contents


# YouTube
YOUTUBE_MAIN_ACTIVITY = 'com.google.android.youtube/com.google.android.apps.youtube.app.WatchWhileActivity'

@keyword(require='youtube')
def open_youtube(device, url):
    start_view(device, url)
    wait_app_focused(device, YOUTUBE_MAIN_ACTIVITY)


@keyword(require='youtube')
@precondition(focused_app(YOUTUBE_MAIN_ACTIVITY))
def youtube_pause(device):
    device.shell(['input', 'keyevent', 'KEYCODE_MEDIA_PAUSE']).wait()


# PlayMusic
PLAYMUSIC_MAIN_ACTIVITY = 'com.google.android.music/com.android.music.activitymanagement.TopLevelActivity'


@keyword(require='playmusic')
def open_playmusic(device):
    device.shell([
        'am', 'start',
        '-a', 'android.intent.activity.MAIN',
        '-n', PLAYMUSIC_MAIN_ACTIVITY]).wait()
    wait_app_focused(device, PLAYMUSIC_MAIN_ACTIVITY)


@keyword(require='playmusic')
@precondition(focused_app(PLAYMUSIC_MAIN_ACTIVITY))
def playmusic_play(device):
    device.uiautomator(resourceId='com.google.android.music:id/play_button').click.wait()


@keyword(require='playmusic')
def playmusic_stop(device):
    device.shell(['input', 'keyevent', 'KEYCODE_MEDIA_STOP']).wait()


# Maps
MAPS_MAIN_ACTIVITY = 'com.google.android.apps.maps/com.google.android.maps.MapsActivity'


@keyword(require='maps')
def open_maps(device, geo_uri):
    device.shell([
        'am', 'start',
        '-a', 'android.intent.action.VIEW',
        '-n', MAPS_MAIN_ACTIVITY,
        '-d', geo_uri]).wait()
    wait_app_focused(device, MAPS_MAIN_ACTIVITY)


@keyword(require='maps')
@precondition(focused_app(MAPS_MAIN_ACTIVITY))
def maps_random_swim(device, duration):
    map_frame = device.uiautomator(resourceId='com.google.android.apps.maps:id/map_frame')
    actions = (
        lambda: map_frame.scroll.vert.forward(),
        lambda: map_frame.scroll.vert.backward(),
        lambda: map_frame.scroll.horiz.forward(),
        lambda: map_frame.scroll.horiz.backward(),
        lambda: map_frame.pinch.In(percent=10),
        lambda: map_frame.pinch.Out(percent=50))
    start_time = time.time()
    while time.time() - start_time < duration:
        random.choice(actions)()
        device.uiautomator.wait.idle()


# Chrome
CHROME_START_ACTIVITY = 'com.android.chrome/com.google.android.apps.chrome.Main'
CHROME_MAIN_ACTIVITY = 'com.android.chrome/org.chromium.chrome.browser.ChromeTabbedActivity'


@keyword(require='chrome')
def open_chrome(device, url):
    device.shell([
        'am', 'start',
        '-n', CHROME_START_ACTIVITY,
        '-d', url]).wait()
    wait_app_focused(device, CHROME_MAIN_ACTIVITY)


@keyword(require='chrome')
@precondition(focused_app(CHROME_MAIN_ACTIVITY))
def chrome_scroll(device):
    scroll_whole_contents(
        device,
        selector={'className': 'android.webkit.WebView'},
        direction='toEnd')

# PlayStore
PLAYSTORE_MAIN_ACTIVITY = 'com.android.vending/com.google.android.finsky.activities.MainActivity'


@keyword(require='playstore')
def playstore_install_app(device, app_id):
    start_view(device, 'market://details?id=' + app_id)
    wait_app_focused(device, PLAYSTORE_MAIN_ACTIVITY)
    device.uiautomator(text='INSTALL').click.wait()
    time.sleep(30)
    device.uiautomator(text='OPEN').wait.exists(timeout=60 * 1000)

