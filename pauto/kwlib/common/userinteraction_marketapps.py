import time
from pauto.keyword import keyword
from pauto.precondition import precondition, focused_app
from .userinteractionutil import start_view, wait_app_focused, scroll_period_of_time


TWITTER_START_ACTIVITY = 'com.twitter.android/.StartActivity'
TWITTER_MAIN_ACTIVITY = 'com.twitter.android/com.twitter.app.main.MainActivity'


@keyword(require='twitter')
def open_twitter(device):
    device.shell([
        'am', 'start',
        '-a', 'android.intent.activity.MAIN',
        '-n', TWITTER_START_ACTIVITY]).wait()
    wait_app_focused(device, TWITTER_MAIN_ACTIVITY)


@keyword(require='twitter')
@precondition(focused_app(TWITTER_MAIN_ACTIVITY))
def twitter_scroll_timeline(device, scroll_duration):
    scroll_period_of_time(
        device,
        selector={'description': 'Home timeline list'},
        duration=scroll_duration)


# Smar Recordert
SMARTRECORDER_MAIN_ACTIVITY = 'com.andrwq.recorder/.RecorderActivity'


@keyword(require='smartrecorder')
def open_soundrecorder(device):
    device.shell([
        'am', 'force-stop',
        'com.andrwq.recorder']).wait()
    device.shell([
        'am', 'start',
        '-n', SMARTRECORDER_MAIN_ACTIVITY]).wait()
    wait_app_focused(device, SMARTRECORDER_MAIN_ACTIVITY)


@keyword(require='smartrecorder')
@precondition(focused_app(SMARTRECORDER_MAIN_ACTIVITY))
def soundrecorder_start_recording(device):
    device.uiautomator(text='Start recording   ').click.wait()


@keyword(require='smartrecorder')
@precondition(focused_app(SMARTRECORDER_MAIN_ACTIVITY))
def soundrecorder_stop_recording(device):
    device.shell(['input', 'keyevent', 'KEYCODE_DPAD_UP']).wait()
    device.shell(['input', 'keyevent', 'KEYCODE_DPAD_DOWN']).wait()
    device.shell(['input', 'keyevent', 'KEYCODE_DPAD_DOWN']).wait()
    device.shell(['input', 'keyevent', 'KEYCODE_DPAD_LEFT']).wait()
    device.shell(['input', 'keyevent', 'KEYCODE_ENTER']).wait()
    time.sleep(5)
    if device.uiautomator(text='OK').exists:
        device.uiautomator(text='OK').click()
