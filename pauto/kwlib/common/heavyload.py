import logging
from pauto.keyword import keyword


@keyword()
def force_low_ram(device, megabytes, used_packages):
    kilobytes = megabytes * 1024
    meminfo = device.call('get_meminfo')
    logging.debug('RAM {available}/{total}'.format(**meminfo))
    if meminfo['available'] < kilobytes:
        return
    while True:
        device.monkey.play_with(used_packages)
        meminfo = device.call('get_meminfo')
        logging.debug('RAM {available}/{total}'.format(**meminfo))
        if meminfo['available'] < kilobytes:
            break


if __name__ == '__main__':
    from pauto.core import pauto_device, model
    from pauto.keywords import deviceinfo  # necessary for get_meminfo
    logging.basicConfig(level=logging.DEBUG)
    device = pauto_device.Device(model.load('emulator'))
    device.force_low_ram(720, used_packages=[
        'com.android.gallery',
        'com.android.quicksearchbox',
        'com.android.browser',
        'com.android.calendar',
        'com.android.webview',
        'com.android.launcher3',
        'com.android.calculator2'])
