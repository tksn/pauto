import re
import time
from pauto.keyword import keyword
from pauto.precondition import precondition, focused_app
from pauto.kwlib.common.userinteractionutil import wait_app_focused


# # Camera
# @keyword(require=['moto_camera'])
# def get_camera_state(device):
#     outs, _ = device.shell(['dumpsys', 'media.camera']).wait()
#     m = re.search(
#         r'Client package: com\.motorola\.cameraone.+Current State: (\d+)', outs,
#         flags=re.MULTILINE | re.DOTALL)
#     if not m:
#         return None
#     return 'record' if m.group(1) == '5' else 'idle'


# CAMERA_MAIN_ACTIVITY = 'com.motorola.cameraone/com.motorola.camera.Camera'


# @keyword(require='moto_camera')
# def open_camera(device):
#     device.shell([
#         'am', 'start',
#         '-a', 'android.intent.activity.MAIN',
#         '-n', CAMERA_MAIN_ACTIVITY,
#         '-f', '0x00008000', '-W']).wait()
#     wait_app_focused(device, CAMERA_MAIN_ACTIVITY)


# @keyword(require='moto_camera')
# @precondition(focused_app(CAMERA_MAIN_ACTIVITY))
# def camera_start_video_recording(device):
#     # Switch to video mode
#     device.uiautomator.click(970, 1650)
#     time.sleep(3)
#     device.uiautomator.click(970, 1290)
#     time.sleep(3)

#     # Tap start button
#     device.uiautomator.click(550, 1660)
#     time.sleep(3)


# @keyword(require='moto_camera')
# @precondition(focused_app(CAMERA_MAIN_ACTIVITY))
# def camera_stop_video_recording(device):
#     # Tap stop button
#     device.uiautomator.click(550, 1660)
#     time.sleep(3)

#     # Switch to still mode
#     device.uiautomator.click(970, 1650)
#     time.sleep(3)
#     device.uiautomator.click(970, 1450)
#     time.sleep(3)

