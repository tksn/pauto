import itertools
import subprocess


class Monkey(object):

    DEFAULT_OPTIONS = [
        '--throttle', '100',
        '--pct-touch', '40',
        '--pct-motion', '40',
        '--pct-appswitch', '20',
        '--pct-syskeys', '0']

    DEFAULT_EVENT_COUNT = 20

    def __init__(self, device):
        self.__device = device

    def run(self, event_count=None, options=None, wait=True):
        ev_cnt = event_count or self.DEFAULT_EVENT_COUNT
        opts = self.DEFAULT_OPTIONS if options is None else options
        args = ['monkey'] + list(opts)
        args.append(str(ev_cnt))
        proc = self.__device.shell(args)
        if wait:
            proc.wait()
        return proc

    def play_with(self, allowed_packages, event_count=None, options=None, wait=True):
        opts = list(
            itertools.chain.from_iterable(('-p', app)
            for app in allowed_packages))
        opts += self.DEFAULT_OPTIONS if options is None else options
        return self.run(event_count, opts, wait)


class Process(object):

    def __init__(self, proc):
        self.__proc = proc

    def wait(self, timeout=None, check=True):
        try:
            outs, errs = self.__proc.communicate(timeout=timeout)
            if check and self.__proc.returncode != 0:
                raise subprocess.CalledProcessError(
                    self.__proc.returncode, self.__proc.args, outs, errs)
        except subprocess.TimeoutExpired:
            self.__proc.kill()
            outs, errs = self.__proc.communicate()
        return outs.decode(), errs.decode()

    def stop(self):
        return self.wait(timeout=0, check=False)


class Device(object):

    MAX_COMMAND_LENGTH = 8192

    def __init__(self, serialno=None):
        self.__serialno = serialno

    @property
    def serialno(self):
        return self.__serialno

    def __start(self, args):
        full_args = ['adb']
        if self.serialno is not None:
            full_args += ['-s', self.serialno]
        full_args += list(args)
        if len(' '.join(full_args)) > self.MAX_COMMAND_LENGTH:
            raise ValueError('length of command arguments exceeds max limit')
        proc = subprocess.Popen(
            full_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return Process(proc)

    def __run(self, args, timeout=None, check=True):
        return self.__start(args).wait(timeout, check)

    def shell(self, args):
        return self.__start(['shell'] + list(args))

    def install(self, apk_path):
        return self.__run(['install', '-r', apk_path])

    def uninstall(self, package):
        return self.__run(['uninstall', package])

    def reboot(self):
        return self.__run(['reboot'])

    @property
    def monkey(self):
        return Monkey(self)
