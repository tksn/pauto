import functools
import re


class PreconditionUnmetError(Exception):
    def __init__(self, expected, actual):
        super().__init__()
        self.expected = expected
        self.actual = actual
    def __str__(self):
        return 'PreconditionUnmet: expected="{}" actual="{}"'.format(self.expected, self.actual)


def focused_app(activity_identifier):
    def check_func(*args, **kwargs):
        focused_app = args[0].get_focused_app()
        if focused_app != activity_identifier:
            raise PreconditionUnmetError(activity_identifier, focused_app)
    return check_func


def verify_condition(check_func, *args, **kwargs):
    check_func(*args, **kwargs)


def precondition(*conditions):
    
    def decorator_func(original_func):

        @functools.wraps(original_func)
        def decorated_func(*args, **kwargs):
            for cond in conditions:
                verify_condition(cond, *args, **kwargs)
            return original_func(*args, **kwargs)
        return decorated_func
    return decorator_func
