_keywordRegistry = {}


def keyword(name=None, require=None):

    def decorator_func(f):
        kwname = name or f.__name__
        kwreq = require or []

        if isinstance(kwreq, str):
            kwreq = [kwreq]

        registry_item = _keywordRegistry.setdefault(kwname, [])
        registry_item.append({
            'func': f,
            'traits': kwreq
        })
        return f
    return decorator_func


def get_keyword_func(name, model, priority_traits=[]):
    candidates = [
        item for item in _keywordRegistry.get(name, [])
        if frozenset(item['traits']) <= frozenset(model['traits'])]
    if not candidates:
        unmatched = [str(item['traits']) for item in _keywordRegistry.get(name, [])]
        raise ValueError('no matching keyword func (unmatched: {})'.format(unmatched))
    top_candidate = max(
        candidates,
        key=lambda c: len(frozenset(priority_traits) & frozenset(c['traits'])))
    return top_candidate['func']

    