from setuptools import setup

setup(name='pauto',
      version='0.1',
      description='Phone automation tools',
      author='tksn',
      author_email='tksnaoi@gmail.com',
      license='MIT',
      packages=['pauto'],
      zip_safe=False)
